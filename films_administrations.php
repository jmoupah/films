<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Films
 *
 * @plugin     Films
 * @copyright  2022
 * @author     GNU/GPL v3
 * @licence    GNU/GPL v3
 * @package    SPIP\Films\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin Films.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function films_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = array(array('maj_tables', array('spip_films', 'spip_films_liens')));

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Films.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function films_vider_tables($nom_meta_base_version) {

	sql_drop_table('spip_films');
	sql_drop_table('spip_films_liens');

	# Nettoyer les liens courants (le génie optimiser_base_disparus se chargera de nettoyer toutes les tables de liens)
	sql_delete('spip_documents_liens', sql_in('objet', array('film')));
	sql_delete('spip_mots_liens', sql_in('objet', array('film')));
	sql_delete('spip_auteurs_liens', sql_in('objet', array('film')));
	# Nettoyer les versionnages et forums
	sql_delete('spip_versions', sql_in('objet', array('film')));
	sql_delete('spip_versions_fragments', sql_in('objet', array('film')));
	sql_delete('spip_forum', sql_in('objet', array('film')));

	effacer_meta($nom_meta_base_version);
}

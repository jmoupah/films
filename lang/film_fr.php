<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_film' => 'Ajouter ce film',

	// C
	'champ_age_explication' => 'A partir de',
	'champ_age_label' => 'Age',
	'champ_annee_label' => 'Année',
	'champ_couleurnb_label' => 'Couleur / N&B',
	'champ_couleurnb_couleur' => 'Couleur',
	'champ_critiques_label' => 'Critiques',
	'champ_couleurnb_nb' => 'N&B',
	'champ_distribution_label' => 'Distribution',
	'champ_duree_label' => 'Durée',
	'champ_genre_label' => 'Genre',
	'champ_jeunepublic_label' => 'Jeune public',
	'champ_jeunepublic_label_case' => 'Ce film est à destination du jeune public',
	'champ_parlantmuet_label' => 'Parlant/Muet',
	'champ_parlantmuet_muet' => 'Muet',
	'champ_parlantmuet_parlant' => 'Parlant',
	'champ_pays_label' => 'Pays',
	'champ_realisation_label' => 'Réalisation',
	'champ_resume_label' => 'Résumé, bande annonce, récompenses...',
	'champ_titre_label' => 'Titre',
	'champ_vovf_label' => 'VOST/VF',
	'champ_vovf_vf' => 'VF',
	'champ_vovf_vo' => 'VOST',
	'confirmer_supprimer_film' => 'Confirmez-vous la suppression de ce film ?',

	// I
	'icone_creer_film' => 'Créer un film',
	'icone_modifier_film' => 'Modifier ce film',
	'info_1_film' => 'Un film',
	'info_aucun_film' => 'Aucun film',
	'info_films_auteur' => 'Les films de cet auteur',
	'info_nb_films' => '@nb@ films',

	// R
	'retirer_lien_film' => 'Retirer ce film',
	'retirer_tous_liens_films' => 'Retirer tous les films',

	// S
	'supprimer_film' => 'Supprimer ce film',

	// T
	'texte_ajouter_film' => 'Ajouter un film',
	'texte_changer_statut_film' => 'Ce film est :',
	'texte_creer_associer_film' => 'Créer et associer un film',
	'texte_definir_comme_traduction_film' => 'Ce film est une traduction du film numéro :',
	'titre_film' => 'Film',
	'titre_films' => 'Films',
	'titre_films_rubrique' => 'Films de la rubrique',
	'titre_langue_film' => 'Langue de ce film',
	'titre_logo_film' => 'Logo de ce film',
	'titre_objets_lies_film' => 'Liés à ce film',
	'titre_page_films' => 'Les films',
);

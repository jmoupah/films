<?php
/**
 * Fonctions du squelette associé
 *
 * @plugin     Films
 * @copyright  2022
 * @author     GNU/GPL v3
 * @licence    GNU/GPL v3
 * @package    SPIP\Films\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


// pour initiale et afficher_initiale
include_spip('prive/objets/liste/auteurs_fonctions');

<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Films
 * @copyright  2022
 * @author     GNU/GPL v3
 * @licence    GNU/GPL v3
 * @package    SPIP\Films\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function films_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['films'] = 'films';

	// Traitements typo et raccourcis
	$interfaces['table_des_traitements']['RESUME'][]= _TRAITEMENT_RACCOURCIS;
	$interfaces['table_des_traitements']['CRITIQUES'][]= _TRAITEMENT_RACCOURCIS;

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function films_declarer_tables_objets_sql($tables) {

	$tables['spip_films'] = array(
		'type' => 'film',
		'principale' => 'oui',
		'field'=> array(
			'id_film'            => 'bigint(21) NOT NULL',
			'titre'              => 'text NOT NULL DEFAULT ""',
			'duree'              => 'tinytext NOT NULL DEFAULT ""',
			'annee'              => 'tinytext NOT NULL DEFAULT ""',
			'genre'              => 'text NOT NULL DEFAULT ""',
			'pays'               => 'text NOT NULL DEFAULT ""',
			'vovf'               => 'tinytext NOT NULL DEFAULT ""',
			'parlantmuet'        => 'tinytext NOT NULL DEFAULT ""',
			'couleurnb'          => 'tinytext NOT NULL DEFAULT ""',
			'jeunepublic'        => 'tinytext NOT NULL DEFAULT ""',
			'age'                => 'text NOT NULL DEFAULT ""',
			'realisation'        => 'text NOT NULL DEFAULT ""',
			'distribution'       => 'text NOT NULL DEFAULT ""',
			'resume'             => 'text NOT NULL DEFAULT ""',
			'critiques'          => 'text NOT NULL DEFAULT ""',
			'date_publication'   => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"', 
			'maj'                => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_film',
		),
		'titre' => 'titre AS titre, "" AS lang',
		'date' => 'date_publication',
		'champs_editables'  => array('titre', 'duree', 'annee', 'genre', 'pays', 'vovf', 'parlantmuet', 'couleurnb', 'jeunepublic', 'age', 'realisation', 'distribution', 'resume', 'critiques'),
		'champs_versionnes' => array('titre', 'duree', 'annee', 'genre', 'pays', 'vovf', 'parlantmuet', 'couleurnb', 'jeunepublic', 'age', 'realisation', 'distribution', 'resume', 'critiques'),
		'rechercher_champs' => array("titre" => 8, "realisation" => 4),
		'tables_jointures'  => array('spip_films_liens'),

	);

	return $tables;
}


/**
 * Déclaration des tables secondaires (liaisons)
 *
 * @pipeline declarer_tables_auxiliaires
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function films_declarer_tables_auxiliaires($tables) {

	$tables['spip_films_liens'] = array(
		'field' => array(
			'id_film'            => 'bigint(21) DEFAULT "0" NOT NULL',
			'id_objet'           => 'bigint(21) DEFAULT "0" NOT NULL',
			'objet'              => 'varchar(25) DEFAULT "" NOT NULL',
			'vu'                 => 'varchar(6) DEFAULT "non" NOT NULL',
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_film,id_objet,objet',
			'KEY id_film'        => 'id_film',
		)
	);

	return $tables;
}

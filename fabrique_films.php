<?php

/**
 *  Fichier généré par la Fabrique de plugin v7
 *   le 2022-01-20 09:32:28
 *
 *  Ce fichier de sauvegarde peut servir à recréer
 *  votre plugin avec le plugin «Fabrique» qui a servi à le créer.
 *
 *  Bien évidemment, les modifications apportées ultérieurement
 *  par vos soins dans le code de ce plugin généré
 *  NE SERONT PAS connues du plugin «Fabrique» et ne pourront pas
 *  être recréées par lui !
 *
 *  La «Fabrique» ne pourra que régénerer le code de base du plugin
 *  avec les informations dont il dispose.
 *
**/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$data = array (
  'fabrique' => 
  array (
    'version' => 7,
  ),
  'paquet' => 
  array (
    'prefixe' => 'films',
    'nom' => 'Films',
    'slogan' => 'Créer des films',
    'description' => '',
    'logo' => 
    array (
      0 => '',
    ),
    'credits' => 
    array (
      'logo' => 
      array (
        'texte' => 'RemixIcon',
        'url' => 'https://remixicon.com/',
      ),
    ),
    'version' => '0.0.1',
    'auteur' => 'GNU/GPL v3',
    'auteur_lien' => 'https://www.cousumain.info/',
    'licence' => 'GNU/GPL v3',
    'etat' => 'dev',
    'compatibilite' => '[4.0.0;4.0.*]',
    'documentation' => '',
    'administrations' => 'on',
    'schema' => '1.0.0',
    'formulaire_config' => '',
    'formulaire_config_titre' => '',
    'inserer' => 
    array (
      'paquet' => '',
      'administrations' => 
      array (
        'maj' => '',
        'desinstallation' => '',
        'fin' => '',
      ),
      'base' => 
      array (
        'tables' => 
        array (
          'fin' => '',
        ),
      ),
    ),
    'scripts' => 
    array (
      'pre_copie' => '',
      'post_creation' => '',
    ),
    'exemples' => '',
    'saisies_mode' => 'html',
  ),
  'objets' => 
  array (
    0 => 
    array (
      'nom' => 'Films',
      'nom_singulier' => 'Film',
      'genre' => 'masculin',
      'logo' => 
      array (
        0 => '',
        32 => '',
        24 => '',
        16 => '',
        12 => '',
      ),
      'table' => 'spip_films',
      'cle_primaire' => 'id_film',
      'cle_primaire_sql' => 'bigint(21) NOT NULL',
      'table_type' => 'film',
      'champs' => 
      array (
        0 => 
        array (
          'nom' => 'Titre',
          'champ' => 'titre',
          'sql' => 'text NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
            2 => 'obligatoire',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
        1 => 
        array (
          'nom' => 'Durée',
          'champ' => 'duree',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
        2 => 
        array (
          'nom' => 'Année',
          'champ' => 'annee',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
        3 => 
        array (
          'nom' => 'Genre',
          'champ' => 'genre',
          'sql' => 'text NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
        4 => 
        array (
          'nom' => 'Pays',
          'champ' => 'pays',
          'sql' => 'text NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
        5 => 
        array (
          'nom' => 'VO/FV',
          'champ' => 'vovf',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'radio',
          'explication' => '',
          'saisie_options' => 'data=[(#ARRAY{vo,VO,vf,VF})], defaut=vo',
        ),
        6 => 
        array (
          'nom' => 'Parlant/Muet',
          'champ' => 'parlantmuet',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'radio',
          'explication' => '',
          'saisie_options' => 'data=[(#ARRAY{parlant,Parlant,muet,Muet})], defaut=parlant',
        ),
        7 => 
        array (
          'nom' => 'Couleur / N&B',
          'champ' => 'couleurnb',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'radio',
          'explication' => '',
          'saisie_options' => 'data=[(#ARRAY{couleur,Couleur,nb,Noir et blanc})], defaut=couleur',
        ),
        8 => 
        array (
          'nom' => 'Jeune public',
          'champ' => 'jeunepublic',
          'sql' => 'tinytext NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'case',
          'explication' => '',
          'saisie_options' => 'datas=[(#ARRAY{oui,Oui})],
label_case=Ce film est à destination du jeune public,
valeur_oui=oui',
        ),
        9 => 
        array (
          'nom' => 'Age',
          'champ' => 'age',
          'sql' => 'text NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => 'A partir de...',
          'saisie_options' => '',
        ),
        10 => 
        array (
          'nom' => 'Réalisation',
          'champ' => 'realisation',
          'sql' => 'text NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
        11 => 
        array (
          'nom' => 'Distribution',
          'champ' => 'distribution',
          'sql' => 'text NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'input',
          'explication' => '',
          'saisie_options' => '',
        ),
        12 => 
        array (
          'nom' => 'Résumé, bande annonce, récompenses...',
          'champ' => 'resume',
          'sql' => 'text NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'textarea',
          'explication' => '',
          'saisie_options' => 'inserer_barre=edition',
        ),
        13 => 
        array (
          'nom' => 'Critiques',
          'champ' => 'critiques',
          'sql' => 'text NOT NULL DEFAULT \'\'',
          'caracteristiques' => 
          array (
            0 => 'editable',
            1 => 'versionne',
          ),
          'recherche' => '',
          'saisie' => 'textarea',
          'explication' => '',
          'saisie_options' => 'inserer_barre=edition',
        ),
      ),
      'champ_titre' => 'titre',
      'champ_date' => 'date_publication',
      'champ_date_ignore' => '',
      'statut' => 'on',
      'chaines' => 
      array (
        'titre_objets' => 'Films',
        'titre_page_objets' => 'Les films',
        'titre_objet' => 'Film',
        'info_aucun_objet' => 'Aucun film',
        'info_1_objet' => 'Un film',
        'info_nb_objets' => '@nb@ films',
        'icone_creer_objet' => 'Créer un film',
        'icone_modifier_objet' => 'Modifier ce film',
        'titre_logo_objet' => 'Logo de ce film',
        'titre_langue_objet' => 'Langue de ce film',
        'texte_definir_comme_traduction_objet' => 'Ce film est une traduction du film numéro :',
        'titre_\\objets_lies_objet' => 'Liés à ce film',
        'titre_objets_rubrique' => 'Films de la rubrique',
        'info_objets_auteur' => 'Les films de cet auteur',
        'retirer_lien_objet' => 'Retirer ce film',
        'retirer_tous_liens_objets' => 'Retirer tous les films',
        'ajouter_lien_objet' => 'Ajouter ce film',
        'texte_ajouter_objet' => 'Ajouter un film',
        'texte_creer_associer_objet' => 'Créer et associer un film',
        'texte_changer_statut_objet' => 'Ce film est :',
        'supprimer_objet' => 'Supprimer ce film',
        'confirmer_supprimer_objet' => 'Confirmez-vous la suppression de ce film ?',
      ),
      'liaison_directe' => '',
      'table_liens' => 'on',
      'vue_liens' => 
      array (
        0 => 'spip_evenements',
      ),
      'afficher_liens' => 'on',
      'roles' => '',
      'auteurs_liens' => '',
      'vue_auteurs_liens' => '',
      'fichiers' => 
      array (
        'echafaudages' => 
        array (
          0 => 'prive/squelettes/contenu/objets.html',
          1 => 'prive/objets/infos/objet.html',
          2 => 'prive/squelettes/contenu/objet.html',
        ),
        'explicites' => 
        array (
          0 => 'action/supprimer_objet.php',
        ),
      ),
      'autorisations' => 
      array (
        'objets_voir' => '',
        'objet_creer' => '',
        'objet_voir' => '',
        'objet_modifier' => '',
        'objet_supprimer' => '',
        'associerobjet' => '',
      ),
      'boutons' => 
      array (
        0 => 'menu_edition',
        1 => 'outils_rapides',
      ),
    ),
  ),
  'images' => 
  array (
    'paquet' => 
    array (
      'logo' => 
      array (
        0 => 
        array (
          'extension' => 'svg',
          'contenu' => 'PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCIgd2lkdGg9IjI0MCIgaGVpZ2h0PSIyNDAiPjxwYXRoIGZpbGw9Im5vbmUiIGQ9Ik0wIDBoMjR2MjRIMHoiLz48cGF0aCBkPSJNMTIgMjBoOHYyaC04QzYuNDc3IDIyIDIgMTcuNTIzIDIgMTJTNi40NzcgMiAxMiAyczEwIDQuNDc3IDEwIDEwYTkuOTU2IDkuOTU2IDAgMCAxLTIgNmgtMi43MDhBOCA4IDAgMSAwIDEyIDIwem0wLTEwYTIgMiAwIDEgMSAwLTQgMiAyIDAgMCAxIDAgNHptLTQgNGEyIDIgMCAxIDEgMC00IDIgMiAwIDAgMSAwIDR6bTggMGEyIDIgMCAxIDEgMC00IDIgMiAwIDAgMSAwIDR6bS00IDRhMiAyIDAgMSAxIDAtNCAyIDIgMCAwIDEgMCA0eiIvPjwvc3ZnPg==',
        ),
      ),
    ),
    'objets' => 
    array (
      0 => 
      array (
        'logo' => 
        array (
          0 => 
          array (
            'extension' => 'svg',
            'contenu' => 'PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCIgd2lkdGg9IjI0MCIgaGVpZ2h0PSIyNDAiPjxwYXRoIGZpbGw9Im5vbmUiIGQ9Ik0wIDBoMjR2MjRIMHoiLz48cGF0aCBkPSJNMTIgMjBoOHYyaC04QzYuNDc3IDIyIDIgMTcuNTIzIDIgMTJTNi40NzcgMiAxMiAyczEwIDQuNDc3IDEwIDEwYTkuOTU2IDkuOTU2IDAgMCAxLTIgNmgtMi43MDhBOCA4IDAgMSAwIDEyIDIwem0wLTEwYTIgMiAwIDEgMSAwLTQgMiAyIDAgMCAxIDAgNHptLTQgNGEyIDIgMCAxIDEgMC00IDIgMiAwIDAgMSAwIDR6bTggMGEyIDIgMCAxIDEgMC00IDIgMiAwIDAgMSAwIDR6bS00IDRhMiAyIDAgMSAxIDAtNCAyIDIgMCAwIDEgMCA0eiIvPjwvc3ZnPg==',
          ),
          16 => 
          array (
            'extension' => 'svg',
            'contenu' => 'PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCIgd2lkdGg9IjI0MCIgaGVpZ2h0PSIyNDAiPjxwYXRoIGZpbGw9Im5vbmUiIGQ9Ik0wIDBoMjR2MjRIMHoiLz48cGF0aCBkPSJNMTIgMjBoOHYyaC04QzYuNDc3IDIyIDIgMTcuNTIzIDIgMTJTNi40NzcgMiAxMiAyczEwIDQuNDc3IDEwIDEwYTkuOTU2IDkuOTU2IDAgMCAxLTIgNmgtMi43MDhBOCA4IDAgMSAwIDEyIDIwem0wLTEwYTIgMiAwIDEgMSAwLTQgMiAyIDAgMCAxIDAgNHptLTQgNGEyIDIgMCAxIDEgMC00IDIgMiAwIDAgMSAwIDR6bTggMGEyIDIgMCAxIDEgMC00IDIgMiAwIDAgMSAwIDR6bS00IDRhMiAyIDAgMSAxIDAtNCAyIDIgMCAwIDEgMCA0eiIvPjwvc3ZnPg==',
          ),
        ),
      ),
    ),
  ),
);

<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {return;}

# Page film : ne pas afficher les documents joints en bas de page s'ils sont insérés dans le contenu
$GLOBALS['medias_liste_champs']['spip_films'] = 'resume';
$GLOBALS['medias_liste_champs'][''] = 'critiques';